## Sveltekit User Interface

1. Install dependencies

```shell
cd ihub-ui
npm install
```

2. Run the development server

```shell
npm run dev
```

## Hasura

### Running graphql

1. Start the hasura server

```shell
docker compose up -d
````

### Console

Run the following to open the hasura console

```shell
cd hasura
hasura console 
```

### Export Metadata and migrations

1. Export the hasura metadata

```shell
hasura metadata export --admin-secret $HASURA_ADMIN_SECRET --project hasura
```

2. Export the hasura migrations

```shell
hasura migrate create "migration_name" --admin-secret $HASURA_ADMIN_SECRET \
        --project hasura --from-server --database-name public --schema public
```

3. Commit the changes to the repo, and follow normal procedures for merging to the default branch.

### Apply Metadata and Migrations to Production

Run the following to commit the repo changes to production.

```
hasura metadata apply --admin-secret $HASURA_ADMIN_SECRET --project hasura
hasura migrate apply --admin-secret $HASURA_ADMIN_SECRET --project hasura --database-name default
```